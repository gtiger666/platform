/**
 * 
 */
package com.desktop.web.uda.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.desktop.web.core.db.BaseEntity;

/**
 * @author baibai
 *
 */
@TableName("t_node")
public class Node extends BaseEntity {

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    private String title;
    private Long pid;
    private String path;
    private Long sort;

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the pid
     */
    public Long getPid() {
        return pid;
    }

    /**
     * @param pid the pid to set
     */
    public void setPid(Long pid) {
        this.pid = pid;
    }

    /**
     * @return the path
     */
    public String getPath() {
        return path;
    }

    /**
     * @param path the path to set
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * @return the sort
     */
    public Long getSort() {
        return sort;
    }

    /**
     * @param sort the sort to set
     */
    public void setSort(Long sort) {
        this.sort = sort;
    }

}
