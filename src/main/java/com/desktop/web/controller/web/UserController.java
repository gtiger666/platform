/**
 * $Revision$
 * $Date$
 *
 * Copyright (C) https://gitee.com/baibaiclouds/platform loon. All rights reserved.
 * <p>
 * This software is the confidential and proprietary information of loon.
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the agreements you entered into with loon.
 * 
 * Modified history:
 *   Loon  2019年11月2日 下午11:58:03  created
 */
package com.desktop.web.controller.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.desktop.web.core.aop.RightTarget;
import com.desktop.web.core.exception.BusinessException;
import com.desktop.web.core.utils.RequestUtil;
import com.desktop.web.core.web.Result;
import com.desktop.web.service.user.UserService;
import com.desktop.web.service.websession.WebSessionService;
import com.desktop.web.uda.entity.User;

/**
 * 
 *
 * @author baibai
 */
@Controller
public class UserController extends BaseWebController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserService userService;

    @Autowired
    private WebSessionService webSessionService;

    @RequestMapping(value = "/webapi/user/self/updatepw", method = {RequestMethod.POST})
    @ResponseBody
    public Object updateSelfpw(HttpServletRequest request) {
        try {

            Map<String, Object> params = RequestUtil.getBody2(request);
            if (params == null || params.isEmpty()) {
                throw new BusinessException("参数为空");
            }

            String oldpw = params.get("oldpw").toString();
            String newpw = params.get("newpw").toString();
            userService.updateSelfpw(RequestUtil.getUid(), oldpw, newpw);
            return Result.Success();
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/webapi/user/self/info/get", method = {RequestMethod.GET})
    @ResponseBody
    public Object getUserselfInfo(HttpServletRequest request) {

        try {
            return Result.Success(userService.getUserInfo(RequestUtil.getUid()));
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/webapi/user/self/logout", method = {RequestMethod.GET})
    @ResponseBody
    public Object logout(HttpServletRequest request) {

        try {
            webSessionService.logout(RequestUtil.getUid());
            return Result.Success();
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/webapi/user/add", method = {RequestMethod.POST})
    @ResponseBody
    @RightTarget(value = "team_user_manager")
    public Object addUser(HttpServletRequest request) {

        try {
            Map<String, String> params = RequestUtil.getBody(request);
            if (params == null || params.isEmpty()) {
                throw new BusinessException("参数为空");
            }

            User user = userService.addUser(params.get("username"), params.get("selfname"), params.get("password"));
            return Result.Success(user);
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/webapi/user/update", method = {RequestMethod.POST})
    @ResponseBody
    @RightTarget(value = "team_user_manager")
    public Object updateUser(HttpServletRequest request) {

        try {
            Map<String, String> params = RequestUtil.getBody(request);
            if (params == null || params.isEmpty()) {
                throw new BusinessException("参数为空");
            }

            User user = new User();
            user.setId(Long.valueOf(params.get("uid")));
            user.setSelfname(params.get("selfname"));
            userService.updateUser(user);

            return Result.Success();
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/webapi/user/delete", method = {RequestMethod.POST})
    @ResponseBody
    @RightTarget(value = "team_user_manager")
    public Object deleteUser(HttpServletRequest request) {

        try {
            Map<String, String> params = RequestUtil.getBody(request);
            if (params == null || params.isEmpty()) {
                throw new BusinessException("参数为空");
            }

            userService.deleteUser(Long.valueOf(params.get("uid")));
            return Result.Success();
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/webapi/user/password/reset", method = {RequestMethod.POST})
    @ResponseBody
    @RightTarget(value = "team_user_manager")
    public Object passwordReset(HttpServletRequest request) {

        try {
            Map<String, String> params = RequestUtil.getBody(request);
            if (params == null || params.isEmpty()) {
                throw new BusinessException("参数为空");
            }

            User user = new User();
            user.setId(Long.valueOf(params.get("uid")));
            user.setPassword(params.get("password"));

            userService.updateUser(user);
            return Result.Success();
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/webapi/user/role/list/get", method = {RequestMethod.GET})
    @ResponseBody
    @RightTarget(value = "team_user_manager")
    public Object getUsrListForRole(HttpServletRequest request) {

        try {
            Long roleId = Long.valueOf(request.getParameter("roleId"));
            List<User> users = userService.getUserListByRoleid(roleId);
            return Result.Success(users);
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/webapi/user/list/get", method = {RequestMethod.GET})
    @ResponseBody
    @RightTarget(value = "team_user_manager")
    public Object getUsrList(HttpServletRequest request) {

        try {
            List<User> users = userService.getUserList(request.getParameter("username"), request.getParameter("selfname"));
            return Result.Success(users);
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }
}
