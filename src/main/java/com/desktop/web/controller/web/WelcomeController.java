/**
 * $Revision$
 * $Date$
 *
 * Copyright (C) https://gitee.com/baibaiclouds/platform loon. All rights reserved.
 * <p>
 * This software is the confidential and proprietary information of loon.
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the agreements you entered into with loon.
 * 
 * Modified history:
 *   Loon  2019年11月2日 下午11:58:03  created
 */
package com.desktop.web.controller.web;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.desktop.web.core.comenum.Status;
import com.desktop.web.core.exception.BusinessException;
import com.desktop.web.core.utils.RequestUtil;
import com.desktop.web.core.web.Result;
import com.desktop.web.service.config.ConfigService;
import com.desktop.web.service.device.DeviceService;
import com.desktop.web.service.user.UserService;
import com.desktop.web.service.welcome.WelcomeService;

/**
 * 
 *
 * @author baibai
 */
@Controller
public class WelcomeController extends BaseWebController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private WelcomeService welcomeService;

    @Autowired
    private ConfigService configService;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/webapi/welcome/state/get", method = {RequestMethod.GET})
    @ResponseBody
    public Object getState(HttpServletRequest request) {

        try {
            Status state = welcomeService.getSystemState();
            return Result.Success(state);
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/webapi/welcome/systeminit", method = {RequestMethod.POST})
    @ResponseBody
    public Object systemInit(HttpServletRequest request) {

        try {
            Map<String, String> params = RequestUtil.getBody(request);
            if (params == null || params.isEmpty()) {
                return null;
            }

            welcomeService.systemInit(params);
            return Result.Success();
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/webapi/welcome/days", method = {RequestMethod.GET})
    @ResponseBody
    public Object getDays(HttpServletRequest request) {
        try {
            return Result.Success(configService.getInitTimeDays());
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/webapi/welcome/count/info", method = {RequestMethod.GET})
    @ResponseBody
    public Object getDeviceCount(HttpServletRequest request) {
        try {
            Map<String, Integer> countInfo = new HashMap<String, Integer>();
            countInfo.putAll(deviceService.getDeviceCountInfo());

            int userCount = userService.getUserCount();
            countInfo.put("userCount", userCount);
            return Result.Success(countInfo);
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/", method = {RequestMethod.GET})
    public Object gotoWebIndex(HttpServletRequest request, HttpServletResponse response) {
        return new ModelAndView("redirect:/web/index.html");
    }

}
